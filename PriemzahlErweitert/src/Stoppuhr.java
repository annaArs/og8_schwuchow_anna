
public class Stoppuhr {

	static long startzeit;
	static long endzeit;
	
	public static void start(){
		startzeit = System.currentTimeMillis();
	}
	
	public static void stopp(){
		endzeit = System.currentTimeMillis();
	}
	
	public static void reset() {
		startzeit = 0;
		endzeit = 0;
	}
	
	public static long getMillis() {
		long Millis = endzeit - startzeit;
		return Millis;
	}
}
