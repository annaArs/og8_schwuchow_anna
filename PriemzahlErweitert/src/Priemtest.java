
public class Priemtest {

	public static boolean istPriem(long zahl) {
		if(zahl < 2) {
			return false;
		}
		for(long i = 2; i < zahl; i++) {
			if(zahl % i == 0) {
				return false;
			}
		}
		return true;
	}
	public static void main(String[] args) {
		long eineZahl = 9_999_999_929L;
		Stoppuhr.start();
		System.out.println(istPriem(eineZahl));
		Stoppuhr.stopp();
		System.out.println(Stoppuhr.getMillis());
		Stoppuhr.reset();
	}

}
