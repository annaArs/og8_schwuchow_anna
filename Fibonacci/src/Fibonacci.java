public class Fibonacci  {

  // Konstruktor
  Fibonacci(){
  }

  //Rekursive Berechnung der Fibonacci-Zahlen
  long fiboRekursiv(int n){
    if(n <= 1)
      return n;
    else
      return fiboRekursiv(n-1) + fiboRekursiv(n-2);
  }//fiboRekursiv

  //Iterative Berechnung der Fibonacci-Zahlen
  long fiboIterativ(int n){
     long vorletztes = 0, letztes = 1, jetziges =0;
     if(n <= 1)
       return n;
     else {
       for(int i=2; i<=n; i++){
         jetziges = letztes + vorletztes;
         vorletztes = letztes;
         letztes = jetziges;
       }//for
       return jetziges;
     } //else
  }//fiboIterativ

}// Fibonnaci
