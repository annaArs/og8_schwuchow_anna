import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;

public class DateiVerwalter {

	private File file;
	
	public DateiVerwalter(File file) {
		this.file = file;
	}
	
	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while((s = br.readLine()) != null) {
				System.out.println(s);
			}
			br.close();
		}
		catch(Exception e) {
			System.out.println("File nicht vorhanden.");
			e.printStackTrace();
		}
	}
	
	public void schreiben(String s) {
		try {
			FileWriter fw = new FileWriter(this.file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(s);
			bw.flush();
			bw.close();
		}
		catch(IOException e){
			System.out.println("File nicht vorhanden.");
			e.printStackTrace();			
		}
	}
	
	public static void main(String[] args) {
		File file = new File("Primzahlen");
		DateiVerwalter dv = new DateiVerwalter(file);
		dv.schreiben("");
		dv.lesen();
	}

}
