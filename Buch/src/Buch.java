
public class Buch {
	private String Autor;
	private String Titel;
	private String ISBN;
	
	public Buch() {
		super();
	}
	public Buch(String autor, String titel, String iSBN) {
		super();
		Autor = autor;
		Titel = titel;
		ISBN = iSBN;
	}
	
	public String getAutor() {
		return Autor;
	}
	
	public void setAutor(String autor) {
		Autor = autor;
	}
	
	public String getTitel() {
		return Titel;
	}
	
	public void setTitel(String titel) {
		Titel = titel;
	}
	
	public String getISBN() {
		return ISBN;
	}
	
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	
}
