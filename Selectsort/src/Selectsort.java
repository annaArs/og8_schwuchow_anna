public class Selectsort {

    //public int[] array = { 16, 23, 14, 7, 21, 20, 6, 1, 17, 13, 12, 9, 3, 19 };

    public static int[] sort(int[]array) {
        int s, k;
        for (int i = array.length - 1; i >= 1; i--) {
            s = 0;
            for (int j = 1; j <= i; j++) {
                if (array[j] > array[s]) {
                    s = j;
                }
            }
            k = array[s];
            array[s] = array[i];
            array[i] = k;
        }
        return array;
    }

/*  public static void main(String[] args) {
        Selectsort sort = new Selectsort();
       int[] arr = sort.sort(int[]array);
        for (int i = 0; i < array.length; i++) {
            System.out.println(i + 1 + ": " + array[i]);
        }
    }*/
} 
