import java.util.HashSet;
import java.util.ArrayList;

public class KeyStore03 {
	HashSet<String> keyStore = new HashSet<String>();
	
	public boolean add(String e) {
		keyStore.add(e);
		return true;
	}
	
	public int indexOf(String e) {
		int i = new ArrayList<>(keyStore).indexOf(e);
		return i;
	}
	
	public void remove(int index) {
		keyStore.remove(index);
	}
}
