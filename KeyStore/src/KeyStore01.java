
public class KeyStore01 {
	private String[] keyStore = new String[20];
	
	public boolean add(String e) {
		for(int i = 0; i < keyStore.length; i++) {
			if(keyStore[i] == null) {
				keyStore[i] = e;
				return true;
			}
		}
		return false;
	}
	
	public int indexOf(String e) {
		for(int i = 0; i < keyStore.length; i++) {
			if(keyStore[i].equals(e)) {
				return i;
			}
		}
		return -1;
	}
	
	public void remove(int index) {
		keyStore[index] = null;
		for(int i = index; i < keyStore.length-1; i++) {
			keyStore[i] = keyStore[i+1];
		}
	}
}
