import java.util.ArrayList;

public class KeyStore02 {
	ArrayList<String> liste = new ArrayList<String>();

	public boolean add(String e) {
		liste.add(e);
		return true;
	}
	
	public int indexOf(String e) {
		return liste.indexOf(e);
	}
	
	public void remove(int index) {
		liste.remove(index);
	}
}
