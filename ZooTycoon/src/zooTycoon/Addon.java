package zooTycoon;

public class Addon {

	private String bezeichnung;
	private int idNummer;
	private double preis;
	private int aktBestand;
	private int maxBestand;

	public Addon(String bezeichnung, int idNummer, double preis, int aktBestand, int maxBestand) {
		this.bezeichnung = bezeichnung;
		this.idNummer = idNummer;
		this.preis = preis;
		this.aktBestand = aktBestand;
		this.maxBestand = maxBestand;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getAktBestand() {
		return aktBestand;
	}

	public void setAktBestand(int aktBestand) {
		this.aktBestand = aktBestand;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}

}
