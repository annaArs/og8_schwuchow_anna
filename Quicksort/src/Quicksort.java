public class Quicksort {

//    public static int[] array = { 16, 23, 14, 7, 21, 20, 6, 1, 17, 13, 12, 9, 3, 19 };

    public static int[] sort(int l, int r, int[] array) {
        int p;
        if (l < r) {
            p = partition(l, r, array);
            sort(l, p, array);
            sort(p + 1, r, array);
        }
        return array;
    }

    public static int partition(int l, int r, int[] array) {

        int s, e, x = array[(l + r) / 2];
        s = l - 1;
        e = r + 1;
        while(true) {
            do {
                s++;
            } 
            while(array[s] < x);
            do {
                e--;
            } while (array[e] > x);

            if (s < e) {
                int k = array[s];
                array[s] = array[e];
                array[e] = k;
            } else {
                return e;
            }
        }
    }

/*   public static void main(String[] args) {
        Quicksort qs = new Quicksort();
        int[] arr = qs.sort(0, array.length - 1);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(" " + arr[i]);
        }
    }*/
} 