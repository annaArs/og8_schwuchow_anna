import java.util.Random;
import java.util.Arrays;

public class QuicksortTest {
	static int[] array = new int[50];
	static Random random = new Random();
	
	public static void main(String[]args) {
		for(int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(100);
		}
		Quicksort.sort(0,array.length-1,array);
		System.out.println(Arrays.toString(array));
	}
	
}
